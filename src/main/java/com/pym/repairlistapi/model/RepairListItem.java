package com.pym.repairlistapi.model;

import com.pym.repairlistapi.enums.AccidentContent;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class RepairListItem {
    private Long id;
    private LocalDate accidentDate;
    private String accidentPlace;
    private String accidentContent;
    private String negligence;
    private String maintenanceDetails;
    private Double totalPayment;
}
