package com.pym.repairlistapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RepairListStaticsResponse {
    private Double allPayment;
    private Double totalRefundPrice;
    private Double ownExpense;
    private Double avgPayment;
    private Double avgRefundPrice;
}
