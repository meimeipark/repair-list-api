package com.pym.repairlistapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AccidentContent {
    CAR_CAR("차대차 사고"),
    CAR_HUMAN("차 대 사람 사고"),
    SINGLE("단독 사고");

    private final String type;
}
