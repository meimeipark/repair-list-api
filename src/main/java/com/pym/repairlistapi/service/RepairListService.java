package com.pym.repairlistapi.service;

import com.pym.repairlistapi.entity.RepairList;
import com.pym.repairlistapi.model.RepairListItem;
import com.pym.repairlistapi.model.RepairListRequest;
import com.pym.repairlistapi.model.RepairListResponse;
import com.pym.repairlistapi.model.RepairListStaticsResponse;
import com.pym.repairlistapi.repository.RepairListRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RepairListService {
    private final RepairListRepository repairListRepository;

    public void setRepairList(RepairListRequest request){
        RepairList addData = new RepairList();
        addData.setAccidentDate(request.getAccidentDate());
        addData.setAccidentPlace(request.getAccidentPlace());
        addData.setAccidentContent(request.getAccidentContent());
        addData.setNegligence(request.getNegligence());
        addData.setRepairShop(request.getRepairShop());
        addData.setMaintenanceDetails(request.getMaintenanceDetails());
        addData.setTotalPayment(request.getTotalPayment());
        addData.setInsuranceApplication(request.getInsuranceApplication());
        addData.setRefundPrice(request.getRefundPrice());
        addData.setAttackerName(request.getAttackerName());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setEtcMemo(request.getEtcMemo());

        repairListRepository.save(addData);
    }

    public List<RepairListItem> getRepairLists(){
        List<RepairList> originList = repairListRepository.findAll();

        List<RepairListItem> result = new LinkedList<>();

        for (RepairList repairList: originList){
            RepairListItem addItem = new RepairListItem();
            addItem.setId(repairList.getId());
            addItem.setAccidentDate(repairList.getAccidentDate());
            addItem.setAccidentPlace(repairList.getAccidentPlace());
            addItem.setAccidentContent(repairList.getAccidentContent().getType());
            addItem.setNegligence(repairList.getNegligence() ? "쌍방과실":"일반과실");
            addItem.setMaintenanceDetails(repairList.getMaintenanceDetails());
            addItem.setTotalPayment(repairList.getTotalPayment());

            result.add(addItem);
        }
        return result;
    }

    public RepairListResponse getRepairList (long id){
        RepairList originData = repairListRepository.findById(id). orElseThrow();

        RepairListResponse response = new RepairListResponse();
        response.setId(originData.getId());
        response.setAccidentDate(originData.getAccidentDate());
        response.setAccidentPlace(originData.getAccidentPlace());
        response.setAccidentContent(originData.getAccidentContent().getType());
        response.setNegligence(originData.getNegligence() ? "쌍방과실":"일반과실");
        response.setRepairShop(originData.getRepairShop());
        response.setMaintenanceDetails(originData.getMaintenanceDetails());
        response.setTotalPayment(originData.getTotalPayment());
        response.setInsuranceApplication(originData.getInsuranceApplication() ? "보험 청구":"보험 미청구");
        response.setRefundPrice(originData.getRefundPrice());
        response.setAttackerName(originData.getAttackerName());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setEtcMemo(originData.getEtcMemo());

        return response;
    }

    public RepairListStaticsResponse getStatics(){
        RepairListStaticsResponse staticsResponse = new RepairListStaticsResponse();

        List<RepairList> originList = repairListRepository.findAll();

        double allPayment = 0D;
        double totalRefundPrice = 0D;
        for (RepairList repairList : originList){
            allPayment += repairList.getTotalPayment();
            totalRefundPrice += repairList.getRefundPrice();
        }

        double ownExpense = allPayment - totalRefundPrice;

        double avgPayment = allPayment / originList.size();
        double avgRefundPrice = totalRefundPrice / originList.size();


        staticsResponse.setAllPayment(allPayment);
        staticsResponse.setTotalRefundPrice(totalRefundPrice);
        staticsResponse.setOwnExpense(ownExpense);
        staticsResponse.setAvgPayment(avgPayment);
        staticsResponse.setAvgRefundPrice(avgRefundPrice);

        return staticsResponse;
    }
}
