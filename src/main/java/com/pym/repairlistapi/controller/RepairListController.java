package com.pym.repairlistapi.controller;


import com.pym.repairlistapi.model.RepairListItem;
import com.pym.repairlistapi.model.RepairListRequest;
import com.pym.repairlistapi.model.RepairListResponse;
import com.pym.repairlistapi.model.RepairListStaticsResponse;
import com.pym.repairlistapi.service.RepairListService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/repair")
public class RepairListController {
    private final RepairListService repairListService;

    @PostMapping("/new")
    public String setRepairList(@RequestBody RepairListRequest request){
        repairListService.setRepairList(request);

        return "등록";
    }

    @GetMapping("/all")
    public List<RepairListItem> getRepairLists(){
        return repairListService.getRepairLists();
    }

    @GetMapping("/detail/{id}")
    public RepairListResponse getRepairList(@PathVariable long id){
        return repairListService.getRepairList(id);
    }

    @GetMapping("/statics")
    public RepairListStaticsResponse getStatics(){
        return repairListService.getStatics();
    }
}
