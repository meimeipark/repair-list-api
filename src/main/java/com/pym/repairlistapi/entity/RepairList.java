package com.pym.repairlistapi.entity;

import com.pym.repairlistapi.enums.AccidentContent;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class RepairList {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDate accidentDate;

    @Column(nullable = false)
    private String accidentPlace;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private AccidentContent accidentContent;

    @Column(nullable = false)
    private Boolean negligence;

    @Column(nullable = false, length = 30)
    private String repairShop;

    @Column(nullable = false, length = 30)
    private String maintenanceDetails;

    @Column(nullable = false)
    private Double totalPayment;

    @Column(nullable = false)
    private Boolean insuranceApplication;

    private Double refundPrice;

    @Column(length = 30)
    private String attackerName;

    @Column(length = 15)
    private String phoneNumber;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;
}
