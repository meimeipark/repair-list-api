package com.pym.repairlistapi.repository;

import com.pym.repairlistapi.entity.RepairList;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepairListRepository extends JpaRepository<RepairList, Long> {
}
